FROM node:alpine3.16
COPY . .
RUN npm install
EXPOSE 9500
CMD npx nodemon
